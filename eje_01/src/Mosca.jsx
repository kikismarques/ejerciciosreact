import React from 'react';
import { GiFly } from "react-icons/gi";
import './mosca.css';

function Mosca (props) {
    return (
      <div className='mosca'>
        <GiFly color={props.color} size="40"/>
      </div>
    )
}

export default Mosca;