import "./bolabingo.css"
import Bola from "./Bola"

function BolaBingo(props) {
    return (
        <Bola num={props.num} />
    )
}

export default BolaBingo;