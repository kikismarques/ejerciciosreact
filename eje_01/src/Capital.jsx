import './capital.css';

function Capital(props) {
    return (
        <div>
            <p className="initial">{props.nom[0]}</p>
            <p className="word">{props.nom}</p>
        </div>
    )
}   

export default Capital;