import './App.css';

import Saluda from "./Saluda";
import Gato from './Gato';
import BolaBingo from './BolaBingo';
import Quadrado from './Quadrado';
import Mosca from './Mosca';
import Capital from './Capital';

function App() {
      let cosa = (
            <div>
                  <Quadrado />
                  <Mosca color="red" />
                  <Capital nom="Barcelona" />
                  <h1>Hola soy React</h1>
                  <Saluda msg="mensaje desde app"numero="10" />
                  <Gato ancho="300" alto="350" nombre="Garfield" />
                  <BolaBingo num="22" />
            </div>
      );
      return cosa;
}

export default App;