import"./gato.css";

function Gato(props){
    let url ="http://placekitten.com/" + props.ancho + "/" + props.alto;

    return (
        <div className="caja-gato">
            <img src={url} alt="gato"/> 
            <hr/>
            <p>{props.nombre}</p>
        </div>
    );
}

export default Gato;