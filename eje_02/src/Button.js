import { useState } from "react";

const Button = () => {
    const [inputValue, setInputValue] = useState(0);    
    const add = () => {
        setInputValue(inputValue + 1);
        if (inputValue >= 10) {setInputValue(10)}
    }
    const sub = () => {
        setInputValue(inputValue - 1);
        if (inputValue <=0) {setInputValue(0)}
    }
     return (
         <div> 
             <button onClick={sub}>-</button>
             <input disabled="disabled" value={inputValue}></input>
             <button onClick={add}>+</button>
         </div>
     )
}

export default Button;