import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { faThumbsDown } from "@fortawesome/free-solid-svg-icons";

import Circulo from "./Circulo";
import Button from "./Button";
import Flipicon from "./Flipicon";
import PageNumbers from "./PageNumbers";
import Lampada from "./Lampada";
import TablaMotos from "./TablaMotos";

let xxx = [
  {
    "id": 1,
    "model": "YAMAHA X-MAX 250",
    "preu": 1300,
    "kilometres": 68000,
    "cilindrada": 249,
    "lloc": "Barcelona",
    "any": 2007
  },
  {
    "id": 2,
    "model": "SUZUKI BURGMAN 200",
    "preu": 1200,
    "kilometres": 45000,
    "cilindrada": 200,
    "lloc": "Barcelona",
    "any": 2007
  },
  {
    "id": 3,
    "model": "BMW",
    "preu": 5600,
    "kilometres": 1000,
    "cilindrada": 800,
    "lloc": "Contagem",
    "any": 2010
  }
]

function App() {
  return (
    <div>
      <Circulo />
      <Circulo />
      <Circulo />
      <Circulo />
      <Circulo />

      <Button />

      <Flipicon icon1={faThumbsUp} icon2={faThumbsDown} />

      <PageNumbers />

      <br />

      <Lampada />

      <TablaMotos datos={xxx} />
    </div>
  );
}

export default App;
