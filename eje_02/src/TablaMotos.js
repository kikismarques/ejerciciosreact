import Table from 'react-bootstrap/Table';

const TablaMotos = props => {
  let totalPrice = 0;
  for (let item of props.datos) {
    totalPrice += item.preu;
  }
  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>id</th>
          <th>model</th>
          <th>preu</th>
          <th>kilometres</th>
          <th>cilindrada</th>
          <th>lloc</th>
          <th>any</th>
        </tr>
      </thead>
      <tbody>
        {props.datos.map(item => {
          return (
            <tr>
              <td>{item.id}</td>
              <td>{item.model}</td>
              <td>{item.preu}</td>
              <td>{item.kilometres}</td>
              <td>{item.cilindrada}</td>
              <td>{item.lloc}</td>
              <td>{item.any}</td>
            </tr>
          )
        })}
      </tbody>
      <tfoot>
        <tr>
          <td></td>
          <td colSpan={2}>Total: {totalPrice}</td>
          <td></td>
        </tr>
      </tfoot>
    </Table>
  )
}




export default TablaMotos;