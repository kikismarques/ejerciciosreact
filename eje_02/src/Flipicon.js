import { useState } from "react";  
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Flipicon = props => {
    const [isSelected, setSelected] = useState(false);
    const handleClick = () => {
        setSelected(!isSelected);
    }
    return (
        <div onClick={handleClick}>
            <FontAwesomeIcon icon={(isSelected && props.icon1) || props.icon2} size="4x" />
        </div>
    )
}

export default Flipicon;