import React, { useState } from "react";
import { Pagination } from "react-bootstrap";

const PageNumber = props => {
    const handleClick = event => {
        alert(event.target.text);
    }
    return (
        <Pagination.Item key={props.num} onClick={handleClick}>
            {props.num}
        </Pagination.Item>
    )
}

const PageNumbers = () => {
    let items = [];
    for (let number = 1; number <= 5; number++) {
        items.push(
            <PageNumber num={number} key={number} />
        );
    }
    const [numbers, setNumbers] = useState(items);
    const handleNext = () => {
        let newItems = [];
        for (let number of numbers) {
            newItems.push(
                <PageNumber num={parseInt(number.key) + 1} key={parseInt(number.key) + 1} />
            )
        }
        if (newItems[4].key <= 20) {
            setNumbers(newItems);
        }
    }
    const handlePrev = () => {
        let newItems = [];
        for (let number of numbers) {
            newItems.push(
                <PageNumber num={parseInt(number.key) - 1} key={parseInt(number.key) - 1} />
            )
        }
        if (newItems[0].key >= 0) {
            setNumbers(newItems);
        }
    }
    return (
        <React.Fragment>
            <Pagination>
                <Pagination.Prev onClick={handlePrev} />
                {numbers}
                <Pagination.Next onClick={handleNext} />
            </Pagination>
        </React.Fragment>
    )
}

export default PageNumbers;