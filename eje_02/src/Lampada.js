import { useState } from "react";

import lampadaOn from "./img/on.jpg";
import lampadaOff from "./img/off.jpg";
import buttonOn from "./img/bon.jpg";
import buttonOff from "./img/boff.jpg";

const Lampada = () => {
    const [isOn, setOn] = useState(false);
    const handleClick = () => {
        setOn(!isOn);
    }
    return (
        <div>
            <img src={(isOn && lampadaOn) || lampadaOff} />
            <img src={(isOn && buttonOn) || buttonOff} onClick={handleClick}/>
        </div>
    )
}

export default Lampada;