import { useState } from "react";
import "./circulo.css";

const Circulo = () => {
    const [isSelected, setSelected] = useState(false);
    const handleClick = () => {
        setSelected(!isSelected);
    }
    return (
        <div className={"circulo " + (isSelected && `selected`)} onClick={handleClick}></div>
    )
}

export default Circulo;